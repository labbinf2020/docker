# What's in here?
    * Docker files with all the dependecies and database included.

# How to use?

    * Clone this project to your local machine using the command bellow:
      git clone git@gitlab.com:labbinf2020/docker.git

    * To set up the docker containers just type on the terminal:
      _UID=${UID} _GID=${GID} docker-compose up

    * To open the database pgadmin or postgres can be used (I advise the use of pgadmin).

## Postgres

    * Username: postgres (is the default)

    * Password: bioinf31

    * You shall connect to the port 15432

## PGAdmin
Follow the instructions bellow


    * 1) Must open your browser and copy and paste this link, http://localhost:16543    
![](pgadmin1.png)

    * 2) You will be asked for the email and password these are:
       email: bioinf@pgadmin.com
       password: bioinf
![](pgadmin2.png)

    * 3) Then open the server section and it will ask for the password
       password: bioinf31
![](pgadmin3.png)

    * 4) You are connected to the server and can acess the database that contains the necessary tables 
        (Datasets, Iqtree and Trees)
![](pgadmin4.png)
